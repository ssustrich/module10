//
//  Building.h
//  Module8
//
//  Created by Shawn J Sustrich on 23/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//

#ifndef __Module8__Building__
#define __Module8__Building__

#include <stdio.h>
#include <vector>
#include "Floor.h"

namespace std {
  class Building {
  private:
    int storey;
    vector<Floor> floors;

  public:
    Building();
    ~Building();
    int getStories(){
      return storey;
    }
    void setStories(int storey);
    Floor getFloor(int storey);
    void serStorey(Floor floor);
    bool hasPassengersWaitingAboveStorey(int floor);
    bool hasPassengersWaitingBelowStorey(int floor);
  };
}

#endif /* defined(__Module8__Building__) */
