//
//  Passenger.h
//  Module8
//
//  Created by Shawn J Sustrich on 16/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//

#ifndef __Module8__Passenger__
#define __Module8__Passenger__

#include <stdio.h>
#include <iostream>
#include <queue>
#include <string>
#include <unordered_set>
#include <deque>
#include <queue>
#include <type_traits>



namespace std {
  class Passenger {
  private:
    int startFloor;
    int endFloor;
    int startTime;
    int endTime;
  public:
    Passenger(int);
    Passenger(int, int);
    virtual ~Passenger();
    void setStartFloor(int);
    void setEndFloor(int);
    int getStartFloor(){
      return startFloor;
    }
    int getEndFloor(){
      return endFloor;
    }
    void DisposeObject();
    void setStartTime(int);
    void setEndTime(int);
    int getStartTime();
    int getEndTime();
  };

} /* namespace std */

#endif /* defined(__Module8__Passenger__) */
